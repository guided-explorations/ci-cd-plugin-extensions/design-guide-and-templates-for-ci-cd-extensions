# A TEMPLATE FOR CI CD EXTENSIONS

A good starting point for building your own.  Includes security specific features and AutoDevOps compatibility.

Please read [EXTENSION-IMPLEMENTATION-TRADEOFFS](./EXTENSION-IMPLEMENTATION-TRADEOFFS.md)

## Rich Tracing - Built In And Your Own Custom Tracing

## Security Scanner Specifics

### Results Format Example That Includes Adding Your Branding
[Sample JSON File in this Project](./myscanner_gl-sast-report.json) which also shows how to customize / brand your scanner in the UI like this:

![Scanner Drop Down](./myscannersast.png)


### Related Development Initiatives:

[[FE] Generic Report Schema: Render 'markdown' (GFM) type on vulnerability details page](https://gitlab.com/gitlab-org/gitlab/-/issues/327386) - improved rendering of text in vulnerabilities.
